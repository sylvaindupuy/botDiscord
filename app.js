const BotManager = require('./components/BotManager.js')
const Discord = require('discord.js')
const os = require('os')

// Variables
const SOUND_PATH = os.platform() == 'win32' ? 'D:\\Téléchargements\\' : '/home/sylvain/sounds/'
const TOKEN = process.env.TOKEN
const client = new Discord.Client()
let servers = new Map()
let bannedUsers = new Array()

function getManager(guild) {
    if (!servers.has(guild)) {
        servers.set(guild, new BotManager(SOUND_PATH))
    }
    return servers.get(guild)
}

client.on('ready', function () {
    console.info("Bot is ready to use - " + new Date())
})

client.on('voiceStateUpdate', (oldMember, newMember) => {
    // Si cela concerne le bot
    if (newMember.user.id == '394935304982757376' && newMember.user.bot) {
        getManager().voiceChannelUpdate(newMember.voiceChannel)
    }
});

client.on('messageReactionAdd', (reaction, user) => {
    getManager().addReactionEvent(reaction, user)
});

client.on('messageReactionRemove', (reaction, user) => {
    getManager().removeReactionEvent(reaction, user)
});

client.on('message', message => {
    let manager = getManager()

    if (message.isMentioned(client.user) && userNotBanned(message.author))
        message.reply('Que puis-je faire pour vous ? (!help pour plus d\'informations)')
    else if (message.content.startsWith('!play') && message.content.split(' ').length >= 2  && userNotBanned(message.author))
        manager.handlePlayYoutube(message)
    else if (message.content === '!leave' && userNotBanned(message.author))
        manager.leaveVoiceChannel()
    else if (message.content === '!skip' && userNotBanned(message.author))
        manager.stopMusic(message)
    else if ((message.content.startsWith('!soundBox') || message.content.startsWith('!sb')) && message.content.split(' ').length === 2  && userNotBanned(message.author))
        manager.playSoundBox(message)
    else if (message.content.startsWith('!soundBoxList') || message.content.startsWith('!sbl') && userNotBanned(message.author))
        manager.soundBoxList(message)
    else if (message.content === '!playlist' && userNotBanned(message.author))
        manager.playlist(message)
    else if (message.content === '!help' && userNotBanned(message.author))
        manager.help(message)
    else if (message.content === '!join' && userNotBanned(message.author))
        manager.join(message)
    else if (message.content.startsWith('!volume') && message.content.split(' ').length === 2 && userNotBanned(message.author))
        manager.setVolume(message.content.split(' ')[1])
    else if (isAdmin(message.author) && message.content.startsWith('!ban') && message.content.split(' ').length == 2) {
        if (bannedUsers.indexOf(message.content.split(' ')[1]) == -1) bannedUsers.push(message.content.split(' ')[1]);
    }
    else if (isAdmin(message.author) && message.content.startsWith('!unban') && message.content.split(' ').length == 2) {
        const index = bannedUsers.indexOf(message.content.split(' ')[1]);
        if (index !== -1) bannedUsers.splice(index, 1);
    }
})

function userNotBanned(user) {
    if(bannedUsers.includes(user.id)) {
        user.send(':warning: You are banned from this app ! :warning:');
        return false;
    };

    return true;
}

function isAdmin(user) {
    return user.id == '211639540354514944';
}

client.login(TOKEN)