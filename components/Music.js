class Music {
    constructor(url, author, channel, guild) {
        this.url = url
        this.author = author
        this.channel = channel
        this.guild = guild
        this.startTime = this.specificStart()
    }

    specificStart() {
        let start = '0s'
        // Si un temps de départ est spécifié
        if (this.url.search("t=") != -1) {
            // On recupere le timer
            start = this.url.split('t=')[1];
            // On recupere le dernier caractère ...
            var dernierChar = start.slice(-1);
            // Si c'est un nombre (ie sans s, m ou h)
            if (dernierChar >= '0' && dernierChar <= '9') {
                // on met en seconde par défaut
                start = start + "s";
            } else {
                // Si ce n'est ni en seconde, ni en minute ni en heure
                if (dernierChar != "s" && dernierChar != "m" && dernierChar != "h") 
                    start = '0s'
            }
        }
        return start
    }
}

module.exports = Music