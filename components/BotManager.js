const fs = require('fs')
const yt = require('ytdl-core')
const Music = require('./Music')
const Vote = require('./Vote')
const validator = require('validator')
const Axios = require('axios')

const GOOGLE_API = 'https://www.googleapis.com/youtube/v3/'
const GOOGLE_API_KEY = process.env.GOOGLE_API_KEY

const FIFTEEN_MINUTE = 900000

class BotManager {

    constructor(pathToSoundBox) {
        this.isPlaying = false
        this.voiceChannel = undefined
        this.volume = 0.1
        this.dispatcher = undefined
        this.currentVote = undefined
        this.isPlayingSoundBox = false
        this.path = pathToSoundBox
        this.youtubePlaylist = new Array()
        this.disconnectTimeout = undefined
    }

    voiceChannelUpdate(newChannel) {
        this.voiceChannel = newChannel
    }

    setVolume(volume) {
        if (volume >= 8) volume = 8
        if (volume < 0) volume = 0
        this.volume = volume / 10
        if (this.isPlaying || this.isPlayingSoundBox) {
            this.dispatcher.setVolume(this.volume)
        }
    }

    leaveVoiceChannel() {
        if (this.voiceChannel && !this.isPlaying && !this.isPlayingSoundBox) {
            this.voiceChannel.leave()
            this.voiceChannel = undefined
        }
    }

    join(message) {
        if (message.member.voiceChannel) {
            if (message.member.voiceChannel != this.voiceChannel) {
                this.voiceChannelUpdate(message.member.voiceChannel)
                this.voiceChannel.join()
            }
        }
    }

    createVote(message) {
        this.currentVote = new Vote(message.content.split('!createVote ')[1], message.channel)
    }

    addReactionEvent(reaction, user) {
        if (this.currentVote && user.id != '394935304982757376') {
            if (this.currentVote.message == reaction.message) {
                if (!(reaction.emoji.name == '👍' || reaction.emoji.name == '👎') || this.currentVote.votants.indexOf(user) != -1) {
                    reaction.remove(user)
                } else {
                    this.currentVote.votants.push(user)
                }
            }
        }
    }

    removeReactionEvent(reaction, user) {
        if (this.currentVote && user.id != '394935304982757376') {
            if (this.currentVote.message == reaction.message) {
                if (reaction.emoji.name == '👍' || reaction.emoji.name == '👎') {
                    this.currentVote.votants.splice(this.currentVote.votants.indexOf(user), 1)
                }
            }
        }
    }

    soundBoxList(message) {
        let list = ''
        fs.readdirSync(this.path).forEach(file => {
            if (file.split('.')[1] === 'mp3')
                list += '\n' + file.split('.')[0]
        })
        message.channel.send(list)
    }

    help(message) {
        message.author.send("```css" + "\n" +
            "Liste des Commandes :" + "\n\n" +
            " - !play <url youtube> : lance la musique dans votre chan vocal" + "\n" +
            " - !play <mots clé> : rechercher une video youtube puis la lancer" + "\n" +
            " - !skip : passe à la musique suivante" + "\n" +
            " - !soundBox (ou !sb) <nom du fichier> : permet de jouer des sons depuis des fichers mp3" + "\n" +
            " - !soundBoxList (ou !sbl) : liste des sons disponibles" + "\n" +
            " - !leave : permet au bot de quitter le chan vocal si il n'est pas en cours d'utilisation" + "\n" +
            " - !join : permet au bot de rejoindre le chan vocal si vous êtes dans un chan différent" + "\n" +
            " - !volume <[0..8]> : permet de regler le volume du bot (entre 0 et 8)" + "\n" +
            " - !playlist : affiche la playlist" + "\n" + "\n" +
            " <> = required information " + "\n" +
            " Do not include <> in your command input.```")
    }

    handlePlayYoutube(message) {
        if (message.content.split(' ').length == 2 && validator.isURL(message.content.split(' ')[1], {require_protocol: true})) {
            this.playYoutubeMusic(message)
        } else {
            this.searchFromGoogleAPI(message)
        }
    }

    searchFromGoogleAPI(message) {
        // remove "!play" from message
        const request = message.content.slice(6, message.content.length)
        let self = this

        if (GOOGLE_API_KEY === undefined) {
            return message.channel.send(':warning: La clé pour l\'api google n\'est pas présente !')
        }

        Axios.get(GOOGLE_API + "search?part=snippet&type=video&key=" + GOOGLE_API_KEY + "&q=" + request).then(function (response) {
            if (response.data.pageInfo.totalResults === 0) {
                return message.channel.send(':warning: No result found !')
            }
            // launch first result
            const videoId = response.data.items[0].id.videoId
            self.playYoutubeMusic(message, "https://www.youtube.com/watch?v=" + videoId)
          })
          .catch(function (error) {
            console.error(error);
            message.channel.send(':warning: Erreur lors de la recherche !')
          })
    }

    playYoutubeMusic(message, url = undefined) {
        // On récupère les arguments de la commande
        if(url === undefined)
            url = message.content.split(' ')[1]

        // Si l'utilisateur ne se trouve pas dans un chan vocal
        if (!message.member.voiceChannel)
            return message.channel.send(':warning: Tu dois te trouver dans un channel vocal !')

        // Si le lien n'est pas un lien youtube
        if (!this.isYoutubeLink(url))
            return message.channel.send(':warning: ' + url + ' : n\'est pas un lien youtube')

        // Si c'est un lien youtube "BE", on remplace
        if (this.isYoutubeBELink(url))
            url = url.replace("https:\/\/youtu.be\/", "https:\/\/www.youtube.com\/watch?v=")

        // Si l'on est pas en cours de lecture
        if (!this.isPlaying && !this.isPlayingSoundBox) {
            // On met à jour le channel vocal
            this.voiceChannel = message.member.voiceChannel
            this.playMusic(new Music(url, message.author.username, message.channel, message.guild.name))
        } else {
            // Sinon on ajoute dans la playlist
            this.youtubePlaylist.push(new Music(url, message.author.username, message.channel, message.guild.name))
            message.reply('le son à été ajouté a la playlist')
        }
    }

    stopMusic() {
        if (this.isPlaying || this.isPlayingSoundBox) {
            this.dispatcher.end()
        }
    }

    playMusic(music) {
        // On join le channel vocal
        this.voiceChannel.join().then(connection => {
            // On met le bool à vrai
            this.isPlaying = true
            clearTimeout(this.disconnectTimeout)
            // On "play" le stream en on récupère le dispatcher
            this.dispatcher = connection.playStream(
                yt(music.url, { audioonly: true, begin: music.startTime }),
                { volume: this.volume })
            
            // Affichage des informations de la vidéo
            yt.getInfo(music.url, function (err, info) {
                const title = info.title
                const duree = info.length_seconds

                console.info(`${new Date().toLocaleString("fr-FR")} [${music.guild}] ${music.author} : ${title}`)

                music.channel.send(`${music.author} a lancé la musique :  \`${title}\``)
                
                if (music.startTime != '0s')
                    music.channel.send(`La musique démarre à : \`${music.startTime}\` (ALPHA)`)
                
                music.channel.send(`Durée: \`${Math.floor(duree / 60)} minutes et ${duree % 60} secondes\` `)
            })

            // Quand la musique est terminée
            this.dispatcher.on('end', () => {
                let self = this

                if (this.youtubePlaylist.length == 0) {
                    this.isPlaying = false
                    this.disconnectTimeout = setTimeout(() => self.leaveVoiceChannel(), FIFTEEN_MINUTE)
                } else {
                    // On joue le premier élément du tableau (et on le suppprime) ...
                    this.playMusic(self.youtubePlaylist.shift())
                }
            })

        }).catch(console.error);
    }

    playlist(message) {
        for (let indice = 0; indice < this.youtubePlaylist.length; indice++) {
            let element = this.youtubePlaylist[indice]
            yt.getInfo(element.url, function (err, info) {
                message.channel.send(indice + 1 + " - " + info.title + " (" + element.author + ")")
            })
        }
    }

    playSoundBox(message) {
        // Si l'utilisateur ne se trouve pas dans un chan vocal
        if (!message.member.voiceChannel)
            return message.channel.send(':warning: Tu dois te trouver dans un channel vocal !')

        if (!this.isPlaying && !this.isPlayingSoundBox) {
            let fileName
            const name = message.content.split(' ')[1]
            fs.readdirSync(this.path).forEach(file => {
                fileName = file.split(".")[0];
                if (name === fileName) {
                    this.isPlayingSoundBox = true
                    // On recupere le chanel audio dans lequel se trouve l'utilisateur
                    this.voiceChannel = message.member.voiceChannel;
                    this.voiceChannel.join().then(connection => {
                        this.dispatcher = connection.playFile(this.path + file);
                        this.dispatcher.setVolume(this.volume)
                        // Quand la musique est terminée
                        this.dispatcher.on('end', () => {
                            this.isPlayingSoundBox = false
                            if (this.youtubePlaylist.length > 0) {
                                // On joue le premier élément du tableau ...
                                this.playMusic(this.youtubePlaylist.shift())
                            }
                        })
                    })
                    return
                }
            })
        }
    }

    isYoutubeLink(url) {
        let p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        return (url.match(p)) ? RegExp.$1 : false;
    }

    isYoutubeBELink(url) {
        let p = /(http|https):\/\/www.youtu.be\/"/;
        return (url.match(p)) ? RegExp.$1 : false;
    }

}

module.exports = BotManager