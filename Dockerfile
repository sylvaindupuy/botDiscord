FROM node:8-alpine

# Create app directory
WORKDIR /usr/src/app

# install needed package
RUN apk update && apk upgrade && apk add git python g++ make ffmpeg

# copy 'package.json'
COPY package.json /usr/src/app

# install dependencies and pm2 (keep application online)
RUN npm install && npm install pm2 -g

# copy project files
COPY . .

# launch app on startup
CMD ["sh", "-c", "pm2-runtime app.js"]