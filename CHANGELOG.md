
## v1.3.0

### Feature

 - Auto disconnect
 - improve logs
 - can ban users

### Performance

 - improve music change speed

## v1.2.3

### Fix

 - tag latest image

## v1.2.2

### Fix

 - fix command to push latest

## v1.2.1

### Fix

 - push latest image (docker hub)

## v1.2.0

### Feature

 - use token from ENV
 - **CI/CD** : Create Merge Request on release
 - add CHANGELOG

### Fix

 - **CI/CD** : exclude tags
 - fix Dockerfile to use files (no git clone)
 - fix README

### Performance

 - **CI/CD** : use node alpine + remove usage of "latest"

## v1.1.0

### Feature

 - Search youtube video fron google API
 - **CI/CD** : Setup release workflow

## v1.0.0

 - Initial release
