

:warning: **DEPRECATED** :warning:

**Find new code -->** [here](https://gitlab.com/sylvaindupuy/bot-discord-ts)

# Bot discord

To run this application, you need a token (of your bot) from discord ([plus d'infos](https://discordapp.com/developers))

## Google Youtube Search API

to use the keyword search functionality, you need to get a api key.
More informations [HERE](https://developers.google.com/youtube/v3/docs/search)

## Run application

### Setup env variables

*linux*

```bash
export GOOGLE_API_KEY='your-api-key'
export TOKEN='yourToken'
```

*Windows*

```bat
SET GOOGLE_API_KEY=your-api-key
SET TOKEN=yourToken
```

### Launch

```bash
node app.js
```

## Run on docker

`docker run -e 'TOKEN'='yourToken' -e 'GOOGLE_API_KEY'='your-api-key' sylvaindupuy/node-bot-discord:x.x.x`

## Changelog

find changelog [here](CHANGELOG.md)

> Sylvain Dupuy
